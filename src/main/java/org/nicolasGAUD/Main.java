package org.nicolasGAUD;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        // Create a Scanner object
        Scanner sc = new Scanner(System.in);
        System.out.println("Entrez un mot ou une phrase : ");
        // Read user input
        String STRING_TO_CHECK = sc.nextLine().toLowerCase();
        System.out.println("Entrez un caractère : ");
        // Converts a string to a character array
        char[] CHAR_TO_COMPARE = sc.nextLine().toLowerCase().toCharArray();
        // the char array will have only one char at index 0
        int charPos = 0;
        Functions functions = new Functions();
        functions.checkPlace(STRING_TO_CHECK, CHAR_TO_COMPARE[charPos]);
    }
}
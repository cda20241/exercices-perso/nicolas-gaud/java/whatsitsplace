package org.nicolasGAUD;

public class Functions {
    String [] ORDINALS = {"première",
            "deuxième",
            "troisième",
            "quatrième",
            "cinquième",
            "sixième",
            "septième",
            "huitième",
            "neuvième",
            "dixième"};


    /*
    :param: Un mot ou une phrase et un caractère entrés par l'utilisateur
    :return: none
    */
    public void checkPlace(String sentence, char c){
        boolean ispresent = false;

        if (!sentence.isEmpty()){
            char[] charList = sentence.toCharArray();
            for (int i = 0 ; i < charList.length ; i++){
                if (charList[i] == c){
                    getOrdinal(i, c);
                    ispresent = true;
                }
            }
            if (!ispresent){
                System.out.println("Le caractère a n\'est pas présent dans la phrase.");
            }
        }
    }


    /*
    :param: a strictly positive integer input by the user
    :this function formats a string by retrieving the literal month based on an index
    :return: none
    */
    public void getOrdinal(int index, char c){
        try{
            System.out.println("On retrouve le caractère \"" + c + "\" à la " + ORDINALS[index] + " position.");
        }
        catch (NumberFormatException e){
            System.out.println("TypeError: L'index spécifié n'est pas un nombre !");
        }
        if (index <= 0){
            throw new IllegalArgumentException("Le nombre doit être positif et entier !");
        }

    }
}
